package com.introlabsystems.scraper.texas;

import com.introlabsystems.scraper.texas.driver.WebDriverWrapper;
import com.introlabsystems.scraper.texas.parser.JsoupManager;

import java.util.logging.Level;
import java.util.logging.Logger;

import static com.introlabsystems.scraper.texas.utils.Constant.BASE_URL;

public class Main {

    private static final Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {

        WebDriverWrapper webDriverWrapper =
                new WebDriverWrapper(BASE_URL);

        webDriverWrapper.startInBrowser();

        String dataFromTable = new JsoupManager()
                .fetchDataFromTable(webDriverWrapper.getSourceHtml());

        logger.log(Level.INFO, dataFromTable);

        webDriverWrapper.closeWebDriver();
    }


}
