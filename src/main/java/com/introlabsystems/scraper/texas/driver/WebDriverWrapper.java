package com.introlabsystems.scraper.texas.driver;

import com.introlabsystems.scraper.texas.utils.Constant;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


public class WebDriverWrapper {

    @Getter
    private String baseUrl;

    private WebDriver webDriver;

    public WebDriverWrapper(String baseUrl) {
        this.baseUrl = baseUrl;
        initWebDriver();
    }

    private void initWebDriver() {
        System.setProperty(Constant.PROPERTY_CHROME_KEY, Constant.PROPERTY_CHROME_VALUE);

        // creating settings for WebDriver
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized"); // open a window in full screen mode
        options.addArguments("--no`-sandbox"); // just need

        webDriver = new ChromeDriver(options);
    }

    public void startInBrowser() {
        if (webDriver == null) {
            initWebDriver();
        }
        webDriver.get(baseUrl);
    }


    public String getSourceHtml() {
        return webDriver.getPageSource();
    }


    public void closeWebDriver() {
        if (webDriver != null) webDriver.close(); // close browser`s window which was called by driver
    }
}
