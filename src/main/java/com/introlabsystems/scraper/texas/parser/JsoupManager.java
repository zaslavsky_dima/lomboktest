package com.introlabsystems.scraper.texas.parser;

import com.introlabsystems.scraper.texas.model.Case;
import com.introlabsystems.scraper.texas.utils.JacksonConverter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class JsoupManager {

    /*good practice*/
    private static final String TABLE_SELECTOR = "table";
    private static final String TABLE_ROW_SELECTOR = "tbody tr";
    private static final String TABLE_COLUMN_SELECTOR = "td";
    private static final String LINK_SELECTOR = "a";
    private static final String LINK_ATTR_SELECTOR = "href";

    public String fetchDataFromTable(String sourceHtml) {

        List<Case> caseList = new ArrayList<>();

        Document jsoupDocument = Jsoup.parse(sourceHtml);
        Element table = jsoupDocument.select(TABLE_SELECTOR).first();
        Elements rows = table.select(TABLE_ROW_SELECTOR); // spreading table into rows

        for (Element row : rows) {
            Elements columns = row.select(TABLE_COLUMN_SELECTOR); // spreading rows into columns by tag

            /*MUST HAVE*/
            Case caseObj = new Case();
            caseObj.setUrl(columns.select(LINK_SELECTOR).attr(LINK_ATTR_SELECTOR));
            caseObj.setCaseId(columns.first().text());
            caseObj.setCaseName(columns.last().text());

            caseList.add(caseObj);

        }

        return JacksonConverter.getInstance().objectToJsonString(caseList);

    }


}
