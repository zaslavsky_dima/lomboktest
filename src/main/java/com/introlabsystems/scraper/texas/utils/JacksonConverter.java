package com.introlabsystems.scraper.texas.utils;


import com.introlabsystems.scraper.texas.model.Case;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

public class JacksonConverter {

    private static JacksonConverter instance =  new JacksonConverter();
    private static final String ERROR_MESSAGE = "Not found list of cases";
    private final Logger logger = Logger.getLogger(JacksonConverter.class.getName());

    public static JacksonConverter getInstance() {
        return instance;
    }


    private JacksonConverter() {
    }

    private final ObjectMapper mapper = new ObjectMapper();

    public String objectToJsonString(Case object) {

        try {
            return mapper.writeValueAsString(object);
        } catch (IOException e) {
            logger.info(ERROR_MESSAGE);
        }
        return null;
    }

    public String objectToJsonString(List<Case> object) {

        try {
            return mapper.writeValueAsString(object);
        } catch (IOException e) {
            logger.info(ERROR_MESSAGE);
        }
        return null;
    }

    public Case jsonStringToObject(String string) {

        try {
            return mapper.readValue(string, Case.class);
        } catch (IOException e) {
            return new Case();
        }
    }

}
