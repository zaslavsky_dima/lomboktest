package com.introlabsystems.scraper.texas.utils;

public final class Constant {

    private Constant() { // with SonarLint
        throw new IllegalStateException("Utility class");
    }


    public static final String BASE_URL = "http://search.txcourts.gov/ebriefs.aspx?coa=cossup";
    public static final String PROPERTY_CHROME_KEY = "webdriver.chrome.driver";

    /*MUST HAVE*/
    public static final String PROPERTY_CHROME_VALUE = System.getProperty("user.dir")
            + "/src/main/resources/chromedriver";

}
