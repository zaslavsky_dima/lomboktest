package com.introlabsystems.scraper.texas.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data // creating data class
@NoArgsConstructor
@AllArgsConstructor // creating default constructor
public class Case {

    private String url;
    private String caseId;
    private String caseName;


}
